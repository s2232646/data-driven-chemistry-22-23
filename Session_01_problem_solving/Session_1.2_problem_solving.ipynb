{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "$$\\require{mhchem}$$\n",
    "# Session 1 - Problem Solving\n",
    "\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\"><img alt=\"Creative Commons Licence\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by/4.0/88x31.png\" title='This work is licensed under a Creative Commons Attribution 4.0 International License.' align=\"right\"/></a>\n",
    "\n",
    "Author: Dr James Cumby   \n",
    "Email: james.cumby@ed.ac.uk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Programming is almost entirely about problem solving, *i.e.* how do you take a complex problem and break it down in to manageable steps that a computer can perform. Whilst this is a useful skill for programming and data analysis, it is much more generally applicable, both within your degree and beyond. Importantly, problem solving is a skill that has to be learned, and this session (and course in general) will try to develop this skill."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 1.2 Solving a problem\n",
    "There are a number of steps involved in solving a problem:\n",
    "1. Understand what the problem is and what it is asking for\n",
    "    - Do you have enough information to solve it immediately?\n",
    "2. Understand what the correct solution needs to be capable of (or equally not capable of)\n",
    "3. Work out a series of steps to get from start to finish \n",
    "    - 'Solve the problem'!\n",
    "4. Check that the solution works as expected"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "This session will look at steps 1-3 and give you practice in breaking down complex problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Import libraries\n",
    "We need a few additional Python features ('Packages', see session 4) in this session - make sure to run the following cell!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import sys\n",
    "import os.path\n",
    "sys.path.append(os.path.abspath('../'))\n",
    "from helper_functions.mentimeter import Mentimeter\n",
    "from helper_functions.formatting import format_pseudocode\n",
    "from IPython.display import IFrame"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Understanding the problem and its solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Some problems have very clear goals, and once you have got used to them are relatively straighforward to solve, i.e.\n",
    "> Find the value of x for which\n",
    ">\n",
    ">$$x - y = 6$$\n",
    "> and\n",
    ">$$2x + y = 18.$$\n",
    "\n",
    "Even if a large number of steps are involved, the process is well-defined.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "In contrast, some questions are much less defined, and these are quite challenging to overcome. Sometimes this is due to an uncertain objective, while sometimes there is a shortage of information.\n",
    "\n",
    "> How would you synthesise 2,3-Dimethyl-2-cyclopenten-1-one from readily-available starting materials?\n",
    "> ![2,3-Dimethyl-2-cyclopenten-1-one structure](./images/dimethyl_2_cyclopenten_1_one.png)\n",
    "(You'll see this in year 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Step 1: Aim(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "The first step of any problem is understanding what you are required to do, and working out whether you have all of the information required to solve it. Consider the following question and then vote in the poll below.\n",
    "\n",
    "> Cheese is acidic, due to the presence of lactic acid. When cheese melts it can separate into milk solids and fat; this can be avoided by keeping the pH of the cheese mixture around 5.2. How much citric acid and/or sodium citrate must be added to cheese to prevent it from separating during melting?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "cheese_vote = Mentimeter(vote = 'https://www.menti.com/inhgqptgjp')\n",
    "cheese_vote.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Step 2: Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Once you have determined the objective of a problem, you then need to work out if you have the information and knowledge required to solve it. For instance, the following question has a clear goal, but what additional information is required?\n",
    "\n",
    "> If human hair is composed mainly of the protein \u03b1-keratin, estimate the rate of incorporation of amino acid units per follicle per second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "hair_vote = Mentimeter(vote = 'https://www.menti.com/459ubwoehy')\n",
    "hair_vote.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tasks 1.2.1\n",
    "\n",
    "In pairs or groups of three, discuss the objective for the following questions, and any information you may require.\n",
    "\n",
    "1. If you could imagine an electron to have the same mass as the planet Mercury, which planet would have approximately the same mass as the proton?\n",
    "2. Based purely on standard electrode potentials, which simple binary reaction would give the largest overall potential difference vs the standard hydrogen electrode?\n",
    "3. If you placed a crystal of Tourmaline on top of a crystal of Herapathite and looked through them, what might you observe?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Step 3: Constructing an algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Once you have determined the problem and have all the information required, you then need to construct an algorithm (sequence of steps) to get to the answer. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Aside - program construction\n",
    "\n",
    "In general, computer programs consist of very few essential 'building blocks' (you will learn about these throughout the course):\n",
    "\n",
    "\n",
    "Operations | Loops | Decisions\n",
    "---------- | -------- | -----------\n",
    "These are things like adding/multiplying numbers, reading or writing files, displaying a graph, etc. | These allow you to repeat things more than once, for instance iterating over files | Decisions (of IF statements) divert the flow of a program by doing some sort of test\n",
    "![Green rectangle representing an operation](./images/operation_schematic.png) | ![Schematic of a loop](./images/loop_schematic.png) | ![Schematic of a decision operation](./images/decision_schematic.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "These can be combined together to create quite complex algorithms:\n",
    "\n",
    "![Combination of loops, decisions and operations as a schematic](./images/complex_schematic.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "> Hint: If you find this sort of graphical programming helpful to understand algorithm logic, check out [Blockly](https://developers.google.com/blockly)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pseudocode\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Loops and decision statements are normally shown as indented:\n",
    "\n",
    "```\n",
    "for each item in a sequence:\n",
    "    do something\n",
    "```\n",
    "\n",
    "Indents can be nested:\n",
    "\n",
    "```\n",
    "if x is 5:\n",
    "    if y is 10:\n",
    "        do something\n",
    "```\n",
    "\n",
    "This indentation is essential in Python (see session 3)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "The previous examples were a form of 'pseudocode'; a way of writing down an algorithm without worrying about the specific commands required to run correctly. Pseudocode is often more readable than 'real' computer code, and can in theory be translated into any programming language."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "For instance, the following 'pseudocode' describes an algorithm to print any files containing the text 'Benzene'\n",
    "\n",
    "```\n",
    "for each file in a list of files:\n",
    "    open file and read contents\n",
    "    if 'Benzene' is in file contents:\n",
    "        print file name\n",
    "    close file    \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The same algorithm written for Python might look like:\n",
    "\n",
    "``` python\n",
    "for file in list_of_file_names:\n",
    "    file_handle = open(file, 'r')\n",
    "    contents = f.readlines()\n",
    "    if 'Benzene' in contents:\n",
    "        print(file)\n",
    "    file_handle.close()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Choosing an algorithm\n",
    "Often, there are multiple valid solutions to a problem. You should try to appreciate other approaches, but find one that you understand.\n",
    "\n",
    "As a simple example, in your head work out the answer to\n",
    "\n",
    "$$\n",
    "54 + 17\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "How did you do it?\n",
    "- $50 + 10 = 60$, then $60 + 4 + 7 = 71$\n",
    "- $54 + 10 = 64$, then $64 + 7 = 71$\n",
    "- $50 + 17 = 67$, then $67 + 4 = 71$\n",
    "- Something else...?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Algorithm worked example - finding alcohols\n",
    "If you were given 1000 random Infrared spectra from small molecules, how could you determine which ones were alcohols?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "alcohol_vote = Mentimeter(vote = 'https://www.menti.com/f47ebjqjh3')\n",
    "alcohol_vote.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Pseudocode solution\n",
    "\n",
    "",
    "```\n",
    "FOR each spectrum:\n",
    "    Find absorbance at approximately 3000 cm-1\n",
    "    IF absorbance > threshold:\n",
    "        assign as alcohol\n",
    "```\n",
    ""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "Mentimeter(vote = 'https://www.menti.com/aoz8bwsooh').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Solution addressing problems:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "",
    "```\n",
    "FOR each spectrum:\n",
    "    Find absorption for $2600 < \\nu < 3500$\n",
    "    fit background\n",
    "    IF absorption - background > threshold:\n",
    "        assign as alcohol\n",
    "```\n",
    ""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Tasks 1.2.2\n",
    "In your groups, discuss and solve the following problems. Work together with one person controlling the computer - this is called 'pair-programming'.\n",
    "\n",
    "## Task 1.2.2a\n",
    "\n",
    "Some reactions can be monitored in-situ by NMR spectroscopy, by following the growth of a new NMR peak with time. For such a reaction, what order would you need to perform the following steps in order to plot a concentration vs time profile?\n",
    "\n",
    "> Drag the boxes into the correct order, remembering to indent things that should be performed inside the loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "IFrame(' https://parsons.herokuapp.com/puzzle/17312c8d7d1d44348ed1bff8886c54da', 950, 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Task 1.2.2b\n",
    "If you were given a sequence of atomic coordinates during a reaction that were for some reason in the wrong order, how might you try to put them back in the correct sequence? For example, consider the sequence of five steps from an S<sub>N</sub>2 reaction shown below (imagining you had the atomic coordinates):\n",
    "\n",
    "![SN2 Reaction steps](./images/SN2_reaction_steps.png)\n",
    "\n",
    "> Hint: If you know how far each atom must move to get to a different step, the next step along the S<sub>N</sub>2 reaction will be the one with the smallest (total) distance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "IFrame(' https://parsons.herokuapp.com/puzzle/7b69c59b740c4d8e82dcbd2875dd5ffe', 950,500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## Advanced task\n",
    "- Write a pseudocode algorithm to determine the molecular weight from an arbitrary chemical formula, e.g. (CH3)3CBr or CH3C(O)CN."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Recap\n",
    "This session has covered:\n",
    "- How to break down a problem\n",
    "    - Know *what* you are trying to answer\n",
    "    - Determine if you have all the information you need before starting\n",
    "- Constructing an algorithm\n",
    "    - Multiple ways of solving the problem\n",
    "        - as long as it works, *how* isn't important\n",
    "    - Try to think of pitfalls of your solution\n",
    "        - One solution may often be faster, more robust, easier to read, etc..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Feedback\n",
    "Please say what you did and didn't like about this session!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "positive_feedback = Mentimeter(vote='https://www.menti.com/d4sdwwt6er')\n",
    "positive_feedback.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "critical_feedback = Mentimeter(vote='https://www.menti.com/ybjs1a5299')\n",
    "critical_feedback.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}