__help__ = """
A module to allow editing and writing of Jupyter notebooks using Python.

Usage
-----

The main use case is to take a 'Master' notebook and remove blocks of
code or markdown, in order to generate versions for e.g. students or demonstrators.

For more details, see the help: `python jupyter_writer.py --help`

Blocks within notebooks are defined
    in code cells:
        ### begin BLOCK .... ### end BLOCK
    in markdown cells:
        <!-- begin BLOCK --> ... <!-- end BLOCK -->


"""

__author__ = "James Cumby"
__email__ = "james.cumby@ed.ac.uk"

import json
import uuid
import re
import os
    
# We need this to correctly write null into the output json document
null = None

class notebook():
    """ Class to hold Jupyter notebook data structure and write to file. 
    
    
    Initial parameters
    ------------------
    
    assessed : bool, default=True
        Whether the notebook is meant for (auto)assessment using nbgrader
    kernel_name : str, default='python'
        Name of ipython kernel to open notebook with
    kernel_display : str, default='Python'
        `Display Name` of kernel within Jupyter notebook
            
    Notes
    -----
    In order to open properly with nbgrader, a python kernel (and associated display name)
    must be provided to the notebook class on initialisation (although this is not 
    necessary for a basic Jupyter notebook). This may need to be modified depending on
    the environment you use for development.
    
    Example Usage
    -------------
    
    # First, set up notebook
    nb1 = notebook(kernel_name='chem_teaching', kernel_display='Chem Teaching')
    # Add nbgrader 'task' cell
    nb1.add_code("import numpy as np", task=True)
    # Add read-only markdown cell
    nb1.add_md(\
    r\"\"\"
    # This is a heading

    We can write any combination of md into a cell, including maths:
    $$
    \frac{1}{x} = 5
    $$
    \"\"\", locked=True)

    # Write the output to a file
    nb1.write_notebook('test.ipynb')
        
    """
    
    def __init__(self, 
                 assessed=True,
                 kernel_name='python',
                 kernel_display="Python"
                ):
        """ Create a data structure for a simple Jupyter notebook. """
        
        self.notebook = {}
        
        if assessed:
            self.assessed = True

        self.notebook['metadata'] = {"kernelspec": {"language": "python",
                                                    "name": kernel_name,
                                                    "display_name": kernel_display,
                                                   },
                                     "language_info": {"codemirror_mode": {"name": "ipython", "version": 3},
                                                       "file_extension": ".py",
                                                       "mimetype": "text/x-python",
                                                       "name": "python",
                                                       "nbconvert_exporter": "python",
                                                       "pygments_lexer": "ipython3",
                                                       "version": "3.8.5"
                                                       }
                                    }
        self.notebook['nbformat'] = 4
        self.notebook['nbformat_minor'] = 5
        self.notebook['cells'] = []
        
        # WE are creating an assessed notebook, so add additional details
        if self.assessed:
            self.notebook['metadata']['celltoolbar'] = "Create Assignment"
            
            self.notebook['metadata']['toc'] = {'base_numbering': 1,
                                                'nav_menu': {},
                                                'number_sections': True,
                                                'sideBar': True,
                                                'skip_hl_title': False,
                                                'title_cell': 'Table of Contents',
                                                'title_sidebar': 'Contents',
                                                'toc_cell': False,
                                                'toc_position': {},
                                                'toc_section_display': True,
                                                'toc_window_display': False,
                                               }
            
    def nbgrader_cell(self,
                     graded=False,
                     locked=False,
                     solution=False,
                     task=False,
                     points = None,
                     cell_id = None,
                    ):
        """ Configure cell for nbgrader compatibility. """
        
        
        # Check for prohibited combinations
        assert self.assessed, "Notebook must be defined as assessed during initialisation to add nbgrader cells"
        
        
        # Check for different cell types; the order here is important!
        # Check for manually graded task
        if task and not graded:
            assert not solution, "Manually-graded tasks cannot have solution=True. "
            # Force locked to be true
            locked = True
            cell_type='man_task'
        # Check for read-only cell
        elif locked:
            assert not graded and not solution and not task, "Read-only cells must only have locked=True"
            cell_type='read_only'
            
        # Check for manually or auto- graded answer
        elif solution:
            assert not task and not locked 
            if graded:    # Should be manually-graded
                assert points > 0, "Manually-graded answers require points > 0."
                cell_type='man_graded'
            elif not graded:    # Auto-graded answer
                # Grade must be true, but we will change it as needed
                assert points is None, "Auto-graded answers should not have points."         
                cell_type='auto_graded'

            
        # Check for autograder tests
        elif graded and not solution:
            locked = True
            assert points > 0, "Autograder tests must have associated points. "
            cell_type="auto_tests"
        
        elif not graded and not locked and not solution and not task:
            # Normal cell - return without metadata
            return {}
        
        else:
            raise ValueError('Unknown combination for nbgrader cell:\n    graded: {}\n    locked: {}\n    solution: {}\n    task: {}'.format(graded, locked, solution, task))
        
        nbgrader_dict = {}
        
        nbgrader_dict['grade'] = graded
        nbgrader_dict['locked']= locked
        nbgrader_dict['schema_version'] = 3
        nbgrader_dict['solution'] = solution
        nbgrader_dict['task'] = task
        if cell_type in ['man_graded','auto_tests']:
            nbgrader_dict['points'] = points
        
        # Add cell id (but don't check it is unique if user-defined)
        if cell_id is None:
            nbgrader_dict['grade_id'] = str(uuid.uuid1())
        else:
            nbgrader_dict['grade_id'] = cell_id
        
        return nbgrader_dict
        
        
        
    def add_code(self, 
                 data,
                 graded=False,
                 locked=False,
                 solution=False,
                 task=False,
                 points=None,
                ):
        """ Add a code cell to the Jupyter notebook dictionary. """
    
        assert type(data) is str
        cell_dict = dict()

        cell_dict['cell_type'] = "code"
        cell_dict['execution_count'] = null
        cell_dict['metadata'] = {}
        cell_dict['outputs'] = []
        cell_dict['source'] = data

        
        # Check if we are autograded
        if self.assessed and (graded or locked or solution or task):
            cell_dict['metadata']['nbgrader'] = self.nbgrader_cell(graded,
                                                       locked,
                                                       solution,
                                                       task,
                                                       points,
                                                      )
        
        # Add the cell to self.notebook
        self.notebook['cells'].append(cell_dict)
        
    def add_md(self, 
               data,
               graded=False,
               locked=False,
               solution=False,
               task=False,
               points=None
              ):
        """ Add a markdown cell to the Jupyter notebook dictionary. """
    
        assert type(data) is str
        cell_dict = {}

        cell_dict['cell_type'] = 'markdown'
        cell_dict['metadata'] = {}
        cell_dict['source'] = data
        
        # Check if we are autograded
        if self.assessed and (graded or locked or solution or task):
            cell_dict['metadata']['nbgrader'] = self.nbgrader_cell(graded,
                                                       locked,
                                                       solution,
                                                       task,
                                                       points,
                                                      )
        
        # Add the cell to self.notebook
        self.notebook['cells'].append(cell_dict)

    def write_notebook(self, file):
        """ Write notebook data structure to ipynb file. """
        with open(file, 'w') as f:
            json.dump(self.notebook, f, indent=1)   



    def strip_text_blocks(self, source, blockname, replacement=''):
        """ Remove blocks of text from cell sources based on start/end strings.
        
        Any text within a `### begin BLOCKNAME` ... `### end BLOCKNAME`
        block is removed.
        """
        
        # Temporarily join the text with a weird character combination to remove later
        text = ':::'.join(source)
        
        # Use a regex to match the begin/end block
        pattern = re.compile('(### begin {0})+.*?(### end {0})+|(<!-- begin {0} -->)+.*?(<!-- end {0} -->)+'.format(blockname), flags=re.DOTALL | re.IGNORECASE)

        return re.sub(pattern,replacement,text).split(':::')

                
    def clean_source(self, blockname, remove_cell = False, clear_output=False, replacement=''):
        """ Remove source blocks corresponding to `blockname` and optionally remove cell. """
        
        to_remove = []
        
        for cell in self.notebook['cells']:
            assert isinstance(cell['source'], list)
            new_source = self.strip_text_blocks(cell['source'], blockname, replacement=replacement)

            # If the cell is otherwise empty, we can remove the cell
            if re.fullmatch('\s*', ''.join(new_source)) and remove_cell:
                to_remove.append(cell)            
                continue

            # Do we want to remove outputs from modified cells (?)
            if clear_output and 'outputs' in cell:
                cell['outputs'] = []    
                
            # Replace source if different length
            cell['source'] = new_source

        for rcell in to_remove:
            self.notebook['cells'].remove(rcell) 

    def clean_formatting(self, blockname):
        """ Remove unwanted formatting commands in remaining blocks. """
        
        pattern = re.compile('(### begin {0})\s*|(### end {0})\s*|(<!-- begin {0} -->)\s*|(<!-- end {0} -->)\s*'.format(blockname), re.IGNORECASE)
        
        for cell in self.notebook['cells']:
            assert isinstance(cell['source'], list)
                 
            cell['source'] = [re.sub(pattern,'',i) for i in cell['source']]

            
def read_notebook(file):
    """ Read a Jupyter notebook and return a Notebook object """
    
    with open(file,'r') as f:
        data = json.load(f)
    
    JNB = notebook()
    JNB.notebook = data
    return JNB
    
def main(master_file, outfile, version):
    """ Process notebook by removing necessary blocks. """
    
    master = read_notebook(master_file)
    
    if version.lower() == 'demonstrator':
        # Only remove tutor-specific blocks
        master.clean_source('tutor', remove_cell=True)
        # Remove unwanted formatting
        master.clean_formatting('livecode')
        master.clean_formatting('answer')
        master.clean_formatting('silent_answer')
        master.clean_formatting('no_present')
    elif version.lower() == 'student':
        # Remove tutor and answer blocks
        master.clean_source('tutor', remove_cell=True)
        master.clean_source('answer', clear_output=True, replacement='#: Your answer or a result here')
        master.clean_source('silent_answer', remove_cell=True)
        # Remove unwanted formatting
        master.clean_formatting('livecode')
        master.clean_formatting('no_present')
    elif version.lower() == 'tutor':
        # Remove livecode blocks
        master.clean_source('livecode', remove_cell=True, clear_output=True, replacement='#: Live demo here')
        master.clean_source('no_present', remove_cell=True)
        # Remove unwanted formatting
        master.clean_formatting('answer')
        master.clean_formatting('silent_answer')
        master.clean_formatting('tutor')
          
    master.write_notebook(outfile)  
    
if __name__ == '__main__':
    
    import argparse
    
    parser = argparse.ArgumentParser(description=
    """
    Extract relevant sections of a master Jupyter notebook
    and output them to a new notebook file. Blocks of text to
    be removed are delimeted by special formatting strings:
    
    in code cells:
        ### begin BLOCK .... ### end BLOCK
    in markdown cells:
        <!-- begin BLOCK --> ... <!-- end BLOCK -->
        
    The available BLOCK definitions are easily extended, but currently:
        - answer (for task answers)
        - silent_answer (for answers without text substitution)
        - tutor (for tutor-specific code)
        - livecode (for removing blocks for live-coding)
        - no_present (to remove from slideshow view)
        
    Three 'versions' can be created, keeping different formatting blocks:
    
    tutor:        replace 'livecode' with `# Live demo here` text
                  remove 'no_present' blocks
    demonstrator: remove all 'tutor' blocks
    student:      remove 'tutor', 'answer' and 'silent_answer' blocks
    """, formatter_class=argparse.RawTextHelpFormatter,
    )
    
    parser.add_argument("master", type=str, help="Master Jupyter Notebook containing content to extract")
    parser.add_argument("outfile", type=str, help="Output file for formatted notebook version (ignored if version == all")
    parser.add_argument("--version", "-v", type=str, default='tutor', help="Version (student, demonstrator, tutor or all) of notebook to output")
    
    args = parser.parse_args()
    
    if args.version.lower() == 'all':
        for v in ['student', 'demonstrator', 'tutor']:
            path, base = os.path.split(args.master)
            newbase = re.sub('master', v, base)
            output = os.path.join(path, newbase)
            main(args.master, output, v)
    else:
        main(args.master, args.outfile, args.version)
  
        
        
        
        
